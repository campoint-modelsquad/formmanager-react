import React from 'react';
import { Meta, Story } from '@storybook/react';
import { CampointFormsProvider } from '../src';
import FormRenderer from '../src/components/FormRenderer';
import { LangEnum } from '../src/types';
import { FormsProviderProps } from '../src/components/FormsProvider';
import { Container, ThemeProvider } from '@material-ui/core';
import createMuiTheme from '@material-ui/core/styles/createMuiTheme';
import { FormNameEnum } from '../src/graphql';
import { useValuesForm } from '../src/graphql/hooks';

const meta: Meta = {
  title: 'FormRenderer',
  component: FormRenderer,
  argTypes: {
    name: {
      control: {
        type: 'text',
      },
    },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story<FormsProviderProps> = ({ name, lang, userId }) => {
  return (
    <ThemeProvider theme={createMuiTheme({})}>
      <Container>
        <CampointFormsProvider lang={lang}>
          <FormRenderer name={name} userId={userId} />
        </CampointFormsProvider>
      </Container>
    </ThemeProvider>
  );
};

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing
export const Default = Template.bind({});
export const AddressForm = Template.bind({});
export const DocumentsBirthdateAndIDShot = Template.bind({});
export const BillingAddress = Template.bind({});
export const Error = Template.bind({});

Default.args = {
  name: FormNameEnum.DocumentsBirthdateIdShot,
  lang: LangEnum.de,
};
AddressForm.args = { name: 'addressForm', lang: LangEnum.de };
DocumentsBirthdateAndIDShot.args = {
  name: FormNameEnum.DocumentsBirthdateIdShot,
  lang: LangEnum.de,
  userId: '4692886',
};
BillingAddress.args = {
  name: FormNameEnum.BillingAddress,
  lang: LangEnum.de,
  userId: '4692886',
};
Error.args = { name: 'invalidForm', lang: LangEnum.de };
