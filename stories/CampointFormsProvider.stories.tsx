import React from 'react';
import { Meta, Story } from '@storybook/react';
import { CampointFormsProvider, DebugContext } from '../src';

const meta: Meta = {
  title: 'CampointFormsProvider',
  component: CampointFormsProvider,
  argTypes: {
    // children: {
    //   control: {
    //     type: 'text',
    //   },
    // },
  },
  parameters: {
    controls: { expanded: true },
  },
};

export default meta;

const Template: Story = (args) => (
  <CampointFormsProvider {...args}>
    <DebugContext />
  </CampointFormsProvider>
);

// By passing using the Args format for exported stories, you can control the props for a component for reuse in a test
// https://storybook.js.org/docs/react/workflows/unit-testing
export const Default = Template.bind({});

Default.args = {};
