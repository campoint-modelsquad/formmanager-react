import React, { createContext, FC, useContext } from 'react';
import { LangEnum } from '../types';
import {
  useForm as useFormProviderForm,
  useLang as useFormProviderLang,
} from './FormsProviderContext';
import { Form, FormElement, FormInput, FormNameEnum } from '../graphql';
import { inspect } from 'util';
import { filterChildren, sortElements } from './FormElements/util';

interface State {
  form: Form;
  lang?: LangEnum;
}

export const DebugContext: FC = () => {
  const context = useContext(FormRendererContext);

  return (
    <div>
      <h3>Form Renderer Context Debug:</h3>
      <pre>
        {inspect(context, { showHidden: true, sorted: true, depth: 6 })}
      </pre>
    </div>
  );
};

export const FormRendererContext = createContext<State>({
  form: {} as Form,
});

export const useName = (): FormNameEnum =>
  useContext(FormRendererContext).form.name;

export const useLang = (): LangEnum => {
  const lang = useContext(FormRendererContext).lang;
  const formProviderLang = useFormProviderLang();
  return lang || formProviderLang;
};

export const useForm = (): Form | undefined | null =>
  useFormProviderForm(useName());

export const useInputs = (): Array<FormInput> => useForm()?.inputs || [];

export const useElements = (): Array<FormElement> => useForm()?.elements || [];

export const useElementChildren = (
  parentId: string | null
): Array<FormElement> => sortElements(filterChildren(useElements(), parentId));

export const useInputProps = (name: string): FormInput | undefined =>
  useInputs().reduce<FormInput | undefined>(
    (prev, input) => (prev ? prev : input.name === name ? input : undefined),
    undefined
  );
