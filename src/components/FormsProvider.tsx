import React, { FC } from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

import { FormsProviderContext } from './FormsProviderContext';
import { FormComponentsMap, InputComponentsMap, LangEnum } from '../types';
import {
  defaultFormsComponentsMap,
  defaultInputComponentsMap,
} from './defaults';
import { useForm } from '../graphql/hooks';
import { FormNameEnum } from '../graphql';

export interface FormsProviderProps {
  formComponents?: Partial<FormComponentsMap>;
  inputComponents?: Partial<InputComponentsMap>;
  name: FormNameEnum;
  lang: LangEnum;
  userId: string;
}

const client = new ApolloClient({
  uri:
    'http://ak.formmanager-backend.x/api/graphql?XDEBUG_SESSION_START=phpstorm',
  cache: new InMemoryCache(),
});

export const FormsProvider: FC<FormsProviderProps> = ({
  children,
  formComponents,
  inputComponents,
  lang = LangEnum.en,
}) => {
  return (
    <ApolloProvider client={client}>
      <FormsProviderContext.Provider
        value={{
          getForm: useForm,
          lang,
          formComponents: { ...defaultFormsComponentsMap, ...formComponents },
          inputComponents: { ...defaultInputComponentsMap, ...inputComponents },
        }}
      >
        {children}
      </FormsProviderContext.Provider>
    </ApolloProvider>
  );
};
