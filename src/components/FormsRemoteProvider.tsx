import React, { FC } from 'react';
import { ApolloClient, ApolloProvider, InMemoryCache } from '@apollo/client';

import { FormsProviderContext } from './FormsProviderContext';
import { formsConfig } from '../localForms';
import { FormComponentsMap, InputComponentsMap, LangEnum } from '../types';
import {
  defaultFormsComponentsMap,
  defaultInputComponentsMap,
} from './defaults';

export interface FormsProviderProps {
  formComponents?: Partial<FormComponentsMap>;
  inputComponents?: Partial<InputComponentsMap>;
  lang: LangEnum;
}

const client = new ApolloClient({
  uri:
    'http://ak.formmanager-backend.x/api/graphql?XDEBUG_SESSION_START=phpstorm',
  cache: new InMemoryCache(),
});

export const FormsRemoteProvider: FC<FormsProviderProps> = ({
  children,
  formComponents,
  inputComponents,
  lang = LangEnum.en,
}) => {
  return (
    <ApolloProvider client={client}>
      <FormsProviderContext.Provider
        value={{
          lang,
          formsConfig,
          formComponents: { ...defaultFormsComponentsMap, ...formComponents },
          inputComponents: { ...defaultInputComponentsMap, ...inputComponents },
        }}
      >
        {children}
      </FormsProviderContext.Provider>
    </ApolloProvider>
  );
};
