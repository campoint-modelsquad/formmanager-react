import React, { FC } from 'react';
import Error from './FormElements/Error';
import FormElementRenderer from './FormElementRenderer';
import { FormRendererContext } from './FormRendererContext';
import { LangEnum } from '../types';
import { Form, Formik } from 'formik';
import { FormNameEnum } from '../graphql';
import { useError, useForm, useLoading } from './FormsProviderContext';
import { filterChildren, sortElements } from './FormElements/util';
import { useValuesForm } from '../graphql/hooks';

interface FormRendererProps {
  name: FormNameEnum;
  lang?: LangEnum;
  initialValues: {};
  userId: string;
}

const FormRenderer: FC<FormRendererProps> = ({ name, lang, userId }) => {
  const loading = useLoading(name);
  const error = useError(name);
  const form = useForm(name);

  const [formValues = [], { loading: valLoading }] = useValuesForm(
    name,
    userId
  );

  console.log('formValues', formValues, valLoading);

  const initialValues = formValues.reduce((values, next) => {
    return { ...values, [next.name]: [next.value] };
  }, {});

  const elements = sortElements(filterChildren(form?.elements || [], null));

  return loading ? (
    <div>loading</div>
  ) : error ? (
    <Error message={'There was a problem while fetching the form: ' + error} />
  ) : form ? (
    <FormRendererContext.Provider value={{ form, lang }}>
      <Formik
        initialValues={{ ...form?.defaultValues, ...initialValues }}
        onSubmit={form.onSubmit}
      >
        <Form name={name} onChange={(...args) => console.log('onChange', args)}>
          {elements.map((element, idx) => (
            <FormElementRenderer key={idx} {...element} />
          ))}
        </Form>
      </Formik>
    </FormRendererContext.Provider>
  ) : (
    <Error message={'Unknown form with name: ' + name} />
  );
};

export default FormRenderer;
