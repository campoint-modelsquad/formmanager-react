import React, { createContext, FC, useContext } from 'react';
import { FormComponentsMap, InputComponentsMap, LangEnum } from '../types';
import { Form, FormNameEnum } from '../graphql/types';
import { FormQueryResult } from '../graphql/hooks';
import { ApolloError } from '@apollo/client';

interface State {
  formComponents: FormComponentsMap;
  inputComponents: InputComponentsMap;
  lang: LangEnum;
  getForm(name: FormNameEnum): FormQueryResult;
}

export const FormsProviderContext = createContext<State>({
  getForm: () => [undefined, {}],
  formComponents: {} as FormComponentsMap,
  inputComponents: {} as InputComponentsMap,
  lang: LangEnum.en,
});

export const DebugContext: FC = () => {
  const context = useContext(FormsProviderContext);

  return (
    <div>
      <h3>Form Context Debug:</h3>
      <pre>{JSON.stringify(context, null, 2)}</pre>
    </div>
  );
};

export const useLang = (): LangEnum => useContext(FormsProviderContext).lang;

export const useFormComponents = (): FormComponentsMap =>
  useContext(FormsProviderContext).formComponents;

export const useInputComponents = (): InputComponentsMap =>
  useContext(FormsProviderContext).inputComponents;

const useFormQueryResult = (name: FormNameEnum): FormQueryResult =>
  useContext(FormsProviderContext).getForm(name);

export const useForm = (name: FormNameEnum): Form | undefined | null =>
  useFormQueryResult(name)[0];

export const useLoading = (name: FormNameEnum): boolean =>
  useFormQueryResult(name)[1]?.loading || false;

export const useError = (name: FormNameEnum): ApolloError | undefined =>
  useFormQueryResult(name)[1]?.error || undefined;
