import React, { FC } from 'react';
import { useLang } from '../FormsProviderContext';
import { Typography as MUITypography } from '@material-ui/core';
import { FormElementTypography } from '../../graphql';

const Typography: FC<FormElementTypography> = ({ variant, text }) => {
  const lang = useLang();
  return <MUITypography variant={variant}>{text[lang]}</MUITypography>;
};

export default Typography;
