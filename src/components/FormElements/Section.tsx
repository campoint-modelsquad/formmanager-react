import React, { FC } from 'react';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { useLang } from '../FormsProviderContext';
import FormElementRenderer from '../FormElementRenderer';
import { FormElementSection } from '../../graphql';
import { useElementChildren } from '../FormRendererContext';
import { isNil, omitBy } from 'lodash';

const Section: FC<FormElementSection> = ({
  id,
  primaryText,
  secondaryText,
  gridProps,
}) => {
  const lang = useLang();
  const elements = useElementChildren(id);
  return (
    <Grid container item {...omitBy(gridProps, isNil)}>
      <Box>
        {primaryText && (
          <Typography variant={'h6'}>{primaryText[lang]}</Typography>
        )}
        {secondaryText && (
          <Typography variant={'subtitle1'}>{secondaryText[lang]}</Typography>
        )}
        {elements.map(({ type, ...props }, idx) => (
          <FormElementRenderer type={type} key={idx} {...props} />
        ))}
      </Box>
    </Grid>
  );
};

export default Section;
