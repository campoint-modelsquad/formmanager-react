import React, { FC } from 'react';
import { useFormikContext } from 'formik';
import Button from '@material-ui/core/Button';
import { useLang } from '../FormRendererContext';
import { FormElementFormAction } from '../../graphql';

const FormAction: FC<FormElementFormAction> = ({ label }) => {
  const lang = useLang();
  const { submitForm, isSubmitting } = useFormikContext();

  return (
    <Button
      variant="contained"
      color="primary"
      disabled={isSubmitting}
      onClick={submitForm}
    >
      {label[lang]}
    </Button>
  );
};

export default FormAction;
