import React, { FC } from 'react';
import FormElementRenderer from '../FormElementRenderer';
import MUIGrid from '@material-ui/core/Grid';
import { FormElementGrid } from '../../graphql';
import { useElementChildren } from '../FormRendererContext';
import { isNil, omitBy } from 'lodash';

const Grid: FC<FormElementGrid> = ({ id, gridProps }) => {
  const elements = useElementChildren(id);
  return (
    <MUIGrid {...omitBy(gridProps, isNil)}>
      {elements.map(({ type, ...props }, idx) => (
        <FormElementRenderer key={idx} type={type} {...props} />
      ))}
    </MUIGrid>
  );
};

export default Grid;
