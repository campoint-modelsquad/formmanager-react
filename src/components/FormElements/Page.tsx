import React, { FC } from 'react';
import FormElementRenderer from '../FormElementRenderer';
import { FormElementTypeEnum } from '../../types';
import { Paper } from '@material-ui/core';
import { FormElementPage } from '../../graphql';
import { useElementChildren } from '../FormRendererContext';

const Page: FC<FormElementPage> = ({ id }) => {
  const elements = useElementChildren(id);
  return (
    <Paper>
      {elements.map(({ type, ...props }, idx) => (
        <FormElementRenderer key={idx} type={type} {...props} />
      ))}
    </Paper>
  );
};

export default Page;
