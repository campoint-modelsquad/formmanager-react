import { FormElement } from '../../graphql';

export const filterChildren = (
  elements: FormElement[],
  parentId: string | null
): FormElement[] =>
  elements.filter(({ parentId: elParentId }) => parentId === elParentId);

export const sortElements = (elements: FormElement[]): FormElement[] =>
  elements.sort((a, b) => a.pos - b.pos);
