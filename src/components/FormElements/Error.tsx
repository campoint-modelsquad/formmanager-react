import React, { FC } from 'react';
import makeStyles from '@material-ui/core/styles/makeStyles';
import { createStyles } from '@material-ui/core';

interface FormErrorProps {
  message: string;
}

const useStyles = makeStyles(() =>
  createStyles({
    root: {
      border: '1px solid red',
      borderRadius: 4,
      padding: 4,
      color: 'red',
      background: 'white',
    },
  })
);

const Error: FC<FormErrorProps> = ({ message }) => {
  const { root } = useStyles();
  return <div className={root}>{message}</div>;
};

export default Error;
