import React, { FC } from 'react';
import Error from './Error';
import { useInputComponents } from '../FormsProviderContext';
import { useInputProps } from '../FormRendererContext';
import { FormElementInput } from '../../graphql';

const Input: FC<FormElementInput> = ({ inputName, ...props }) => {
  const inputProps = useInputProps(inputName);
  const inputComponents = useInputComponents();

  if (!inputProps) {
    return <Error message={'Unknown input element of name: ' + inputName} />;
  }
  const { type, ...restInputProps } = inputProps;
  const Component = inputComponents[type];
  console.log('Input', inputName, type, props);
  if (!Component) {
    return <Error message={'No renderer configured for type: ' + type} />;
  }

  return (
    <Component {...restInputProps} {...props} name={inputName} type={type} />
  );
};

export default Input;
