import React, { FC } from 'react';
import { FormInputUploadedFile } from '../../graphql';

const UploadedFileInput: FC<FormInputUploadedFile> = ({ label, ...props }) => {
  return <pre>UploadedFile: {JSON.stringify(props, null, 2)}</pre>;
  return <div>UploadedFile</div>;
};

export default UploadedFileInput;
