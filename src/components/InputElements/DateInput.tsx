import React, { FC } from 'react';
import { useLang } from '../FormRendererContext';
import { Field } from 'formik';
import { TextField } from 'formik-material-ui';
import { FormInputDate } from '../../graphql';

const DateInput: FC<FormInputDate> = ({ name, label }) => {
  const lang = useLang();
  const labelLang = label && label[lang];

  return (
    <Field
      component={TextField}
      id={'dateInput-' + name}
      name={name}
      label={labelLang}
    />
  );
};

export default DateInput;
