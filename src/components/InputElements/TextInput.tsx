import React, { FC } from 'react';
import { useLang } from '../FormRendererContext';
import { TextField } from 'formik-material-ui';
import { Field } from 'formik';
import { FormInputText } from '../../graphql';

const TextInput: FC<FormInputText> = ({ name, label }) => {
  const lang = useLang();
  const labelLang = label && label[lang];

  return (
    <Field
      component={TextField}
      id={'textInput-' + name}
      name={name}
      label={labelLang}
    />
  );
};

export default TextInput;
