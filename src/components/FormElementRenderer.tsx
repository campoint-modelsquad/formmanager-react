import React, { FC } from 'react';
import { useFormComponents } from './FormsProviderContext';
import { FormElementTypeEnum } from '../types';
import Error from './FormElements/Error';

interface FormElementRendererProps {
  type: FormElementTypeEnum;
}

const FormElementRenderer: FC<FormElementRendererProps> = ({
  type,
  ...props
}) => {
  const components = useFormComponents();
  const Component = components[type];

  if (!Component) {
    return <Error message={'Unknown form element of type: ' + type} />;
  }

  return <Component {...props} />;
};

export default FormElementRenderer;
