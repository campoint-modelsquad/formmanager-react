import { FormComponentsMap, InputComponentsMap } from '../types';
import { FormElementTypeEnum, FormInputTypeEnum } from '../graphql';
import {
  DateInput,
  DateTimeInput,
  DecimalInput,
  TextInput,
  UploadedFileInput,
} from './InputElements';
import Action from './FormElements/FormAction';
import Grid from './FormElements/Grid';
import Input from './FormElements/Input';
import Page from './FormElements/Page';
import Section from './FormElements/Section';
import Typography from './FormElements/Typography';

export const defaultFormsComponentsMap: FormComponentsMap = {
  [FormElementTypeEnum.FormAction]: Action,
  [FormElementTypeEnum.Grid]: Grid,
  [FormElementTypeEnum.Input]: Input,
  [FormElementTypeEnum.Page]: Page,
  [FormElementTypeEnum.Section]: Section,
  [FormElementTypeEnum.Typography]: Typography,
};

export const defaultInputComponentsMap: InputComponentsMap = {
  [FormInputTypeEnum.Date]: DateInput,
  [FormInputTypeEnum.DateTime]: DateTimeInput,
  [FormInputTypeEnum.Decimal]: DecimalInput,
  [FormInputTypeEnum.MultiLangText]: TextInput,
  [FormInputTypeEnum.Text]: TextInput,
  [FormInputTypeEnum.UploadedFile]: UploadedFileInput,
};
