import { Form, FormElementTypeEnum, FormInputTypeEnum } from './graphql/types';

export type MultiLangText = Record<LangEnum, string>;

export interface FormsConfig {
  [K: string]: Form;
}

export type FormComponentsMap = Record<FormElementTypeEnum, any>;
export type InputComponentsMap = Record<FormInputTypeEnum, any>;

export enum LangEnum {
  de = 'de',
  en = 'en',
}
