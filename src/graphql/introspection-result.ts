/**
 * !!! ATTENTION !!!
 * !!! THIS IS A GENERATED FILE FROM GRAPHQL ENDPOINT, ANY MODIFICATIONS WILL BE OVERWRITTEN !!!
 *
 * to generate this file, please call `npm run codegen`
 *
 */

export type PossibleTypesResultData = {
  possibleTypes: {
    FormInput: [
      'FormInputDate',
      'FormInputDateTime',
      'FormInputDecimal',
      'FormInputMultiLangText',
      'FormInputText',
      'FormInputUploadedFile'
    ];
    FormElement: [
      'FormElementFormAction',
      'FormElementGrid',
      'FormElementInput',
      'FormElementPage',
      'FormElementSection',
      'FormElementTypography'
    ];
  };
};
const result: PossibleTypesResultData = {
  possibleTypes: {
    FormInput: [
      'FormInputDate',
      'FormInputDateTime',
      'FormInputDecimal',
      'FormInputMultiLangText',
      'FormInputText',
      'FormInputUploadedFile',
    ],
    FormElement: [
      'FormElementFormAction',
      'FormElementGrid',
      'FormElementInput',
      'FormElementPage',
      'FormElementSection',
      'FormElementTypography',
    ],
  },
};
export default result;
