/**
 * !!! ATTENTION !!!
 * !!! THIS IS A GENERATED FILE FROM GRAPHQL ENDPOINT, ANY MODIFICATIONS WILL BE OVERWRITTEN !!!
 *
 * to generate this file, please call `npm run codegen`
 *
 */

import { gql } from '@apollo/client';
import { GetFormConfigQuery, GetFormConfigQueryVariables } from './types';

import * as Apollo from '@apollo/client';
export const FormElementGridPropsFragmentDoc = /*#__PURE__*/ gql`
  fragment FormElementGridProps on FormElementGridProps {
    item
    container
    alignContent
    alignItems
    direction
    justify
    spacing
    wrap
    zeroMinWidth
  }
`;
export const FormElementGridFragmentDoc = /*#__PURE__*/ gql`
  fragment FormElementGrid on FormElementGrid {
    gridProps {
      ...FormElementGridProps
    }
  }
  ${FormElementGridPropsFragmentDoc}
`;
export const FormElementInputFragmentDoc = /*#__PURE__*/ gql`
  fragment FormElementInput on FormElementInput {
    inputName
  }
`;
export const FormElementSectionFragmentDoc = /*#__PURE__*/ gql`
  fragment FormElementSection on FormElementSection {
    primaryText
    secondaryText
    gridProps {
      ...FormElementGridProps
    }
  }
  ${FormElementGridPropsFragmentDoc}
`;
export const FormElementTypographyFragmentDoc = /*#__PURE__*/ gql`
  fragment FormElementTypography on FormElementTypography {
    variant
    text
  }
`;
export const FormInputFragmentDoc = /*#__PURE__*/ gql`
  fragment FormInput on FormInput {
    __typename
    type
    name
    label
  }
`;
export const GetFormConfigDocument = /*#__PURE__*/ gql`
  query GetFormConfig($name: FormNameEnum!) {
    forms {
      form(name: $name) {
        __typename
        name
        onSubmit
        inputs {
          __typename
          type
          name
          label
        }
        elements {
          __typename
          id
          type
          parentId
          pos
          ... on FormElementGrid {
            ...FormElementGrid
          }
          ... on FormElementInput {
            ...FormElementInput
          }
          ... on FormElementSection {
            ...FormElementSection
          }
          ... on FormElementTypography {
            ...FormElementTypography
          }
        }
      }
    }
  }
  ${FormElementGridFragmentDoc}
  ${FormElementInputFragmentDoc}
  ${FormElementSectionFragmentDoc}
  ${FormElementTypographyFragmentDoc}
`;

/**
 * __useGetFormConfigQuery__
 *
 * To run a query within a React component, call `useGetFormConfigQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetFormConfigQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetFormConfigQuery({
 *   variables: {
 *      name: // value for 'name'
 *   },
 * });
 */
export function useGetFormConfigQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetFormConfigQuery,
    GetFormConfigQueryVariables
  >
) {
  return Apollo.useQuery<GetFormConfigQuery, GetFormConfigQueryVariables>(
    GetFormConfigDocument,
    baseOptions
  );
}
export function useGetFormConfigLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetFormConfigQuery,
    GetFormConfigQueryVariables
  >
) {
  return Apollo.useLazyQuery<GetFormConfigQuery, GetFormConfigQueryVariables>(
    GetFormConfigDocument,
    baseOptions
  );
}
export type GetFormConfigQueryHookResult = ReturnType<
  typeof useGetFormConfigQuery
>;
export type GetFormConfigLazyQueryHookResult = ReturnType<
  typeof useGetFormConfigLazyQuery
>;
export type GetFormConfigQueryResult = Apollo.QueryResult<
  GetFormConfigQuery,
  GetFormConfigQueryVariables
>;
export function refetchGetFormConfigQuery(
  variables?: GetFormConfigQueryVariables
) {
  return { query: GetFormConfigDocument, variables: variables };
}
export const GetFormValuesDocument = /*#__PURE__*/ gql`
  query GetFormValues($name: FormNameEnum!, $userId: ID!) {
    values {
      form(name: $name, key: $userId) {
        name
        value
      }
    }
  }
`;

/**
 * __useGetFormValuesQuery__
 *
 * To run a query within a React component, call `useGetFormValuesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetFormValuesQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useGetFormValuesQuery({
 *   variables: {
 *      name: // value for 'name'
 *      userId: // value for 'userId'
 *   },
 * });
 */
export function useGetFormValuesQuery(
  baseOptions: Apollo.QueryHookOptions<
    GetFormValuesQuery,
    GetFormValuesQueryVariables
  >
) {
  return Apollo.useQuery<GetFormValuesQuery, GetFormValuesQueryVariables>(
    GetFormValuesDocument,
    baseOptions
  );
}
export function useGetFormValuesLazyQuery(
  baseOptions?: Apollo.LazyQueryHookOptions<
    GetFormValuesQuery,
    GetFormValuesQueryVariables
  >
) {
  return Apollo.useLazyQuery<GetFormValuesQuery, GetFormValuesQueryVariables>(
    GetFormValuesDocument,
    baseOptions
  );
}
export type GetFormValuesQueryHookResult = ReturnType<
  typeof useGetFormValuesQuery
>;
export type GetFormValuesLazyQueryHookResult = ReturnType<
  typeof useGetFormValuesLazyQuery
>;
export type GetFormValuesQueryResult = Apollo.QueryResult<
  GetFormValuesQuery,
  GetFormValuesQueryVariables
>;
export function refetchGetFormValuesQuery(
  variables?: GetFormValuesQueryVariables
) {
  return { query: GetFormValuesDocument, variables: variables };
}
