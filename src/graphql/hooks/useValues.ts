import { FormNameEnum, Value } from '../types';
import { useGetFormValuesQuery } from '../generated';
import { ApolloError } from '@apollo/client';

export type ValuesFormQueryResult = [
  Value[],
  Partial<{ loading: boolean; error: ApolloError | undefined }>
];

export const useValuesForm = (
  name: FormNameEnum,
  userId: string
): ValuesFormQueryResult => {
  console.log('useValuesForm', name, userId);
  const { data, ...result } = useGetFormValuesQuery({
    variables: { name, userId },
  });

  const formValues: Value[] = data?.values.form;
  return [formValues, result];
};
