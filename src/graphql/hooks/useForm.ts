import { Form, FormNameEnum } from '../types';
import { useGetFormConfigQuery } from '../generated';
import { ApolloError } from '@apollo/client';

export type FormQueryResult = [
  Form | undefined | null,
  Partial<{ loading: boolean; error: ApolloError | undefined }>
];

export const useForm = (name: FormNameEnum): FormQueryResult => {
  const { data, ...result } = useGetFormConfigQuery({ variables: { name } });
  const form = data?.forms.form;
  return [form as Form | undefined | null, result];
};
