/**
 * !!! ATTENTION !!!
 * !!! THIS IS A GENERATED FILE FROM GRAPHQL ENDPOINT, ANY MODIFICATIONS WILL BE OVERWRITTEN !!!
 *
 * to generate this file, please call `npm run codegen`
 *
 */

import {
  FieldPolicy,
  FieldReadFunction,
  TypePolicies,
} from '@apollo/client/cache';
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** returns an indexed array with lang as index and the translation as value. Additionally there is a special index "_langs" with an array, holding all valid languages in that context */
  MultiLangText: any;
  Json: any;
  Func: any;
};

export type Query = {
  __typename?: 'Query';
  /** Information about this API */
  api: Api;
  /** Form */
  forms: Forms;
  /** Values */
  values: Values;
};

export type Api = {
  __typename?: 'Api';
  phpVersion?: Maybe<Scalars['String']>;
};

export type Forms = {
  __typename?: 'Forms';
  form?: Maybe<Form>;
};

export type FormsformArgs = {
  name: FormNameEnum;
};

export enum FormNameEnum {
  /** Form for billing address / payout address */
  BillingAddress = 'BillingAddress',
  /** Form with birthdate and binary upload of ID document and ID shot */
  DocumentsBirthdateIdShot = 'DocumentsBirthdateIdShot',
}

export type Form = {
  __typename?: 'Form';
  name: FormNameEnum;
  inputs: Array<FormInput>;
  elements: Array<FormElement>;
  defaultValues: Scalars['Json'];
  /** Current Values of a form */
  values: Array<Value>;
  onSubmit?: Maybe<Scalars['Func']>;
};

export type FormvaluesArgs = {
  key: Scalars['ID'];
};

export type FormInput = {
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export enum FormInputTypeEnum {
  /** Date input */
  Date = 'Date',
  /** DateTime input */
  DateTime = 'DateTime',
  /** Decimal input */
  Decimal = 'Decimal',
  /** MultiLangText input */
  MultiLangText = 'MultiLangText',
  /** Text input */
  Text = 'Text',
  /** UploadedFile input */
  UploadedFile = 'UploadedFile',
}

export type FormElement = {
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
};

export enum FormElementTypeEnum {
  /** renders a form action */
  FormAction = 'FormAction',
  /** renders a grid item and/or container */
  Grid = 'Grid',
  /** renders an previous defined input element */
  Input = 'Input',
  /** renders a page */
  Page = 'Page',
  /** renders a section */
  Section = 'Section',
  /** renders a typography element */
  Typography = 'Typography',
}

export type Value = {
  __typename?: 'Value';
  /** Name, corresponding to name in inputs */
  name: Scalars['ID'];
  /** Value */
  value?: Maybe<Scalars['Json']>;
  /** Value type */
  valueType: ValueTypeEnum;
  /** Value name */
  valueMapping: ValueNameEnum;
};

export enum ValueTypeEnum {
  /** boolean value */
  BOOL = 'BOOL',
  /** date value */
  DATE = 'DATE',
  /** datetime value */
  DATETIME = 'DATETIME',
  /** float value */
  FLOAT = 'FLOAT',
  /** integer value */
  INT = 'INT',
  /** string value */
  STRING = 'STRING',
}

export enum ValueNameEnum {
  /** Model firstname [valueType=STRING] */
  model_address_firstname = 'model_address_firstname',
  /** Model lastname [valueType=STRING] */
  model_address_lastname = 'model_address_lastname',
  /** Model Primary Address City [valueType=STRING] */
  model_address_primary_city = 'model_address_primary_city',
  /** Model Primary Address Company [valueType=STRING] */
  model_address_primary_company = 'model_address_primary_company',
  /** Model Primary Address Country [valueType=STRING] */
  model_address_primary_country = 'model_address_primary_country',
  /** Model Primary Address House Number [valueType=STRING] */
  model_address_primary_house_number = 'model_address_primary_house_number',
  /** Model Primary Address Post Code [valueType=STRING] */
  model_address_primary_post_code = 'model_address_primary_post_code',
  /** Model Primary Address Street [valueType=STRING] */
  model_address_primary_street = 'model_address_primary_street',
  /** Model Birthdate [valueType=DATE] */
  model_birthdate = 'model_birthdate',
  /** Model Document ID Doc [valueType=STRING] */
  model_document_id_doc = 'model_document_id_doc',
  /** Model Document ID Shot [valueType=STRING] */
  model_document_id_shot = 'model_document_id_shot',
}

export type Values = {
  __typename?: 'Values';
  /** Current Values of a form */
  form: Array<Value>;
};

export type ValuesformArgs = {
  name: FormNameEnum;
  key: Scalars['ID'];
};

export type Mutation = {
  __typename?: 'Mutation';
  /** placeholder */
  foo: Scalars['String'];
};

export type FormElementFormAction = FormElement & {
  __typename?: 'FormElementFormAction';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormElementGrid = FormElement & {
  __typename?: 'FormElementGrid';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
  gridProps: FormElementGridProps;
};

export type FormElementGridProps = {
  __typename?: 'FormElementGridProps';
  item?: Maybe<Scalars['Boolean']>;
  container?: Maybe<Scalars['Boolean']>;
  alignContent?: Maybe<Scalars['String']>;
  alignItems?: Maybe<Scalars['String']>;
  direction?: Maybe<Scalars['String']>;
  justify?: Maybe<Scalars['String']>;
  spacing?: Maybe<Scalars['String']>;
  wrap?: Maybe<Scalars['String']>;
  zeroMinWidth?: Maybe<Scalars['String']>;
};

export type FormElementInput = FormElement & {
  __typename?: 'FormElementInput';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
  inputName: Scalars['String'];
};

export type FormElementPage = FormElement & {
  __typename?: 'FormElementPage';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
};

export type FormElementSection = FormElement & {
  __typename?: 'FormElementSection';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
  primaryText?: Maybe<Scalars['MultiLangText']>;
  secondaryText?: Maybe<Scalars['MultiLangText']>;
  gridProps: FormElementGridProps;
};

export type FormElementTypography = FormElement & {
  __typename?: 'FormElementTypography';
  type: FormElementTypeEnum;
  id: Scalars['ID'];
  parentId?: Maybe<Scalars['ID']>;
  pos: Scalars['Int'];
  variant: TypographyVariantEnum;
  text: Scalars['MultiLangText'];
};

export enum TypographyVariantEnum {
  /** Body 1, normal */
  body1 = 'body1',
  /** Body 2, usually dense typing */
  body2 = 'body2',
  /** Button text */
  button = 'button',
  /** Caption text */
  caption = 'caption',
  /** h1 Heading */
  h1 = 'h1',
  /** h2 Heading */
  h2 = 'h2',
  /** h3 Heading */
  h3 = 'h3',
  /** h4 Heading */
  h4 = 'h4',
  /** h5 Heading */
  h5 = 'h5',
  /** h6 Heading */
  h6 = 'h6',
  /** Overline text */
  overline = 'overline',
  /** Subtitle 1 text */
  subtitle1 = 'subtitle1',
  /** Subtitle 1 text, usually smaller, denser */
  subtitle2 = 'subtitle2',
}

export type FormInputDate = FormInput & {
  __typename?: 'FormInputDate';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormInputDateTime = FormInput & {
  __typename?: 'FormInputDateTime';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormInputDecimal = FormInput & {
  __typename?: 'FormInputDecimal';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormInputMultiLangText = FormInput & {
  __typename?: 'FormInputMultiLangText';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormInputText = FormInput & {
  __typename?: 'FormInputText';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormInputUploadedFile = FormInput & {
  __typename?: 'FormInputUploadedFile';
  name: Scalars['ID'];
  type: FormInputTypeEnum;
  label?: Maybe<Scalars['MultiLangText']>;
};

export type FormElementGridFragment = { __typename?: 'FormElementGrid' } & {
  gridProps: {
    __typename?: 'FormElementGridProps';
  } & FormElementGridPropsFragment;
};

export type FormElementInputFragment = {
  __typename?: 'FormElementInput';
} & Pick<FormElementInput, 'inputName'>;

export type FormElementSectionFragment = {
  __typename?: 'FormElementSection';
} & Pick<FormElementSection, 'primaryText' | 'secondaryText'> & {
    gridProps: {
      __typename?: 'FormElementGridProps';
    } & FormElementGridPropsFragment;
  };

export type FormElementTypographyFragment = {
  __typename?: 'FormElementTypography';
} & Pick<FormElementTypography, 'variant' | 'text'>;

export type FormElementGridPropsFragment = {
  __typename?: 'FormElementGridProps';
} & Pick<
  FormElementGridProps,
  | 'item'
  | 'container'
  | 'alignContent'
  | 'alignItems'
  | 'direction'
  | 'justify'
  | 'spacing'
  | 'wrap'
  | 'zeroMinWidth'
>;

type FormInput_FormInputDate_Fragment = { __typename: 'FormInputDate' } & Pick<
  FormInputDate,
  'type' | 'name' | 'label'
>;

type FormInput_FormInputDateTime_Fragment = {
  __typename: 'FormInputDateTime';
} & Pick<FormInputDateTime, 'type' | 'name' | 'label'>;

type FormInput_FormInputDecimal_Fragment = {
  __typename: 'FormInputDecimal';
} & Pick<FormInputDecimal, 'type' | 'name' | 'label'>;

type FormInput_FormInputMultiLangText_Fragment = {
  __typename: 'FormInputMultiLangText';
} & Pick<FormInputMultiLangText, 'type' | 'name' | 'label'>;

type FormInput_FormInputText_Fragment = { __typename: 'FormInputText' } & Pick<
  FormInputText,
  'type' | 'name' | 'label'
>;

type FormInput_FormInputUploadedFile_Fragment = {
  __typename: 'FormInputUploadedFile';
} & Pick<FormInputUploadedFile, 'type' | 'name' | 'label'>;

export type FormInputFragment =
  | FormInput_FormInputDate_Fragment
  | FormInput_FormInputDateTime_Fragment
  | FormInput_FormInputDecimal_Fragment
  | FormInput_FormInputMultiLangText_Fragment
  | FormInput_FormInputText_Fragment
  | FormInput_FormInputUploadedFile_Fragment;

export type GetFormConfigQueryVariables = Exact<{
  name: FormNameEnum;
}>;

export type GetFormConfigQuery = { __typename?: 'Query' } & {
  forms: { __typename?: 'Forms' } & {
    form?: Maybe<
      { __typename: 'Form' } & Pick<Form, 'name' | 'onSubmit'> & {
          inputs: Array<
            | ({ __typename: 'FormInputDate' } & Pick<
                FormInputDate,
                'type' | 'name' | 'label'
              >)
            | ({ __typename: 'FormInputDateTime' } & Pick<
                FormInputDateTime,
                'type' | 'name' | 'label'
              >)
            | ({ __typename: 'FormInputDecimal' } & Pick<
                FormInputDecimal,
                'type' | 'name' | 'label'
              >)
            | ({ __typename: 'FormInputMultiLangText' } & Pick<
                FormInputMultiLangText,
                'type' | 'name' | 'label'
              >)
            | ({ __typename: 'FormInputText' } & Pick<
                FormInputText,
                'type' | 'name' | 'label'
              >)
            | ({ __typename: 'FormInputUploadedFile' } & Pick<
                FormInputUploadedFile,
                'type' | 'name' | 'label'
              >)
          >;
          elements: Array<
            | ({ __typename: 'FormElementFormAction' } & Pick<
                FormElementFormAction,
                'id' | 'type' | 'parentId' | 'pos'
              >)
            | ({ __typename: 'FormElementGrid' } & Pick<
                FormElementGrid,
                'id' | 'type' | 'parentId' | 'pos'
              > &
                FormElementGridFragment)
            | ({ __typename: 'FormElementInput' } & Pick<
                FormElementInput,
                'id' | 'type' | 'parentId' | 'pos'
              > &
                FormElementInputFragment)
            | ({ __typename: 'FormElementPage' } & Pick<
                FormElementPage,
                'id' | 'type' | 'parentId' | 'pos'
              >)
            | ({ __typename: 'FormElementSection' } & Pick<
                FormElementSection,
                'id' | 'type' | 'parentId' | 'pos'
              > &
                FormElementSectionFragment)
            | ({ __typename: 'FormElementTypography' } & Pick<
                FormElementTypography,
                'id' | 'type' | 'parentId' | 'pos'
              > &
                FormElementTypographyFragment)
          >;
        }
    >;
  };
};

export type GetFormValuesQueryVariables = Exact<{
  name: FormNameEnum;
  userId: Scalars['ID'];
}>;

export type GetFormValuesQuery = { __typename?: 'Query' } & {
  values: { __typename?: 'Values' } & {
    form: Array<{ __typename?: 'Value' } & Pick<Value, 'name' | 'value'>>;
  };
};

export type QueryKeySpecifier = (
  | 'api'
  | 'forms'
  | 'values'
  | QueryKeySpecifier
)[];
export type QueryFieldPolicy = {
  api?: FieldPolicy<any> | FieldReadFunction<any>;
  forms?: FieldPolicy<any> | FieldReadFunction<any>;
  values?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ApiKeySpecifier = ('phpVersion' | ApiKeySpecifier)[];
export type ApiFieldPolicy = {
  phpVersion?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormsKeySpecifier = ('form' | FormsKeySpecifier)[];
export type FormsFieldPolicy = {
  form?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormKeySpecifier = (
  | 'name'
  | 'inputs'
  | 'elements'
  | 'defaultValues'
  | 'values'
  | 'onSubmit'
  | FormKeySpecifier
)[];
export type FormFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  inputs?: FieldPolicy<any> | FieldReadFunction<any>;
  elements?: FieldPolicy<any> | FieldReadFunction<any>;
  defaultValues?: FieldPolicy<any> | FieldReadFunction<any>;
  values?: FieldPolicy<any> | FieldReadFunction<any>;
  onSubmit?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputKeySpecifier
)[];
export type FormInputFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | FormElementKeySpecifier
)[];
export type FormElementFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ValueKeySpecifier = (
  | 'name'
  | 'value'
  | 'valueType'
  | 'valueMapping'
  | ValueKeySpecifier
)[];
export type ValueFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  value?: FieldPolicy<any> | FieldReadFunction<any>;
  valueType?: FieldPolicy<any> | FieldReadFunction<any>;
  valueMapping?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ValuesKeySpecifier = ('form' | ValuesKeySpecifier)[];
export type ValuesFieldPolicy = {
  form?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type MutationKeySpecifier = ('foo' | MutationKeySpecifier)[];
export type MutationFieldPolicy = {
  foo?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementFormActionKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | 'label'
  | FormElementFormActionKeySpecifier
)[];
export type FormElementFormActionFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementGridKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | 'gridProps'
  | FormElementGridKeySpecifier
)[];
export type FormElementGridFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
  gridProps?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementGridPropsKeySpecifier = (
  | 'item'
  | 'container'
  | 'alignContent'
  | 'alignItems'
  | 'direction'
  | 'justify'
  | 'spacing'
  | 'wrap'
  | 'zeroMinWidth'
  | FormElementGridPropsKeySpecifier
)[];
export type FormElementGridPropsFieldPolicy = {
  item?: FieldPolicy<any> | FieldReadFunction<any>;
  container?: FieldPolicy<any> | FieldReadFunction<any>;
  alignContent?: FieldPolicy<any> | FieldReadFunction<any>;
  alignItems?: FieldPolicy<any> | FieldReadFunction<any>;
  direction?: FieldPolicy<any> | FieldReadFunction<any>;
  justify?: FieldPolicy<any> | FieldReadFunction<any>;
  spacing?: FieldPolicy<any> | FieldReadFunction<any>;
  wrap?: FieldPolicy<any> | FieldReadFunction<any>;
  zeroMinWidth?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementInputKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | 'inputName'
  | FormElementInputKeySpecifier
)[];
export type FormElementInputFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
  inputName?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementPageKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | FormElementPageKeySpecifier
)[];
export type FormElementPageFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementSectionKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | 'primaryText'
  | 'secondaryText'
  | 'gridProps'
  | FormElementSectionKeySpecifier
)[];
export type FormElementSectionFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
  primaryText?: FieldPolicy<any> | FieldReadFunction<any>;
  secondaryText?: FieldPolicy<any> | FieldReadFunction<any>;
  gridProps?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormElementTypographyKeySpecifier = (
  | 'type'
  | 'id'
  | 'parentId'
  | 'pos'
  | 'variant'
  | 'text'
  | FormElementTypographyKeySpecifier
)[];
export type FormElementTypographyFieldPolicy = {
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  parentId?: FieldPolicy<any> | FieldReadFunction<any>;
  pos?: FieldPolicy<any> | FieldReadFunction<any>;
  variant?: FieldPolicy<any> | FieldReadFunction<any>;
  text?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputDateKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputDateKeySpecifier
)[];
export type FormInputDateFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputDateTimeKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputDateTimeKeySpecifier
)[];
export type FormInputDateTimeFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputDecimalKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputDecimalKeySpecifier
)[];
export type FormInputDecimalFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputMultiLangTextKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputMultiLangTextKeySpecifier
)[];
export type FormInputMultiLangTextFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputTextKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputTextKeySpecifier
)[];
export type FormInputTextFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type FormInputUploadedFileKeySpecifier = (
  | 'name'
  | 'type'
  | 'label'
  | FormInputUploadedFileKeySpecifier
)[];
export type FormInputUploadedFileFieldPolicy = {
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
  label?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type TypedTypePolicies = TypePolicies & {
  Query?: {
    keyFields?:
      | false
      | QueryKeySpecifier
      | (() => undefined | QueryKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: QueryFieldPolicy;
  };
  Api?: {
    keyFields?: false | ApiKeySpecifier | (() => undefined | ApiKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: ApiFieldPolicy;
  };
  Forms?: {
    keyFields?:
      | false
      | FormsKeySpecifier
      | (() => undefined | FormsKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormsFieldPolicy;
  };
  Form?: {
    keyFields?: false | FormKeySpecifier | (() => undefined | FormKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormFieldPolicy;
  };
  FormInput?: {
    keyFields?:
      | false
      | FormInputKeySpecifier
      | (() => undefined | FormInputKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputFieldPolicy;
  };
  FormElement?: {
    keyFields?:
      | false
      | FormElementKeySpecifier
      | (() => undefined | FormElementKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementFieldPolicy;
  };
  Value?: {
    keyFields?:
      | false
      | ValueKeySpecifier
      | (() => undefined | ValueKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: ValueFieldPolicy;
  };
  Values?: {
    keyFields?:
      | false
      | ValuesKeySpecifier
      | (() => undefined | ValuesKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: ValuesFieldPolicy;
  };
  Mutation?: {
    keyFields?:
      | false
      | MutationKeySpecifier
      | (() => undefined | MutationKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: MutationFieldPolicy;
  };
  FormElementFormAction?: {
    keyFields?:
      | false
      | FormElementFormActionKeySpecifier
      | (() => undefined | FormElementFormActionKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementFormActionFieldPolicy;
  };
  FormElementGrid?: {
    keyFields?:
      | false
      | FormElementGridKeySpecifier
      | (() => undefined | FormElementGridKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementGridFieldPolicy;
  };
  FormElementGridProps?: {
    keyFields?:
      | false
      | FormElementGridPropsKeySpecifier
      | (() => undefined | FormElementGridPropsKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementGridPropsFieldPolicy;
  };
  FormElementInput?: {
    keyFields?:
      | false
      | FormElementInputKeySpecifier
      | (() => undefined | FormElementInputKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementInputFieldPolicy;
  };
  FormElementPage?: {
    keyFields?:
      | false
      | FormElementPageKeySpecifier
      | (() => undefined | FormElementPageKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementPageFieldPolicy;
  };
  FormElementSection?: {
    keyFields?:
      | false
      | FormElementSectionKeySpecifier
      | (() => undefined | FormElementSectionKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementSectionFieldPolicy;
  };
  FormElementTypography?: {
    keyFields?:
      | false
      | FormElementTypographyKeySpecifier
      | (() => undefined | FormElementTypographyKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormElementTypographyFieldPolicy;
  };
  FormInputDate?: {
    keyFields?:
      | false
      | FormInputDateKeySpecifier
      | (() => undefined | FormInputDateKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputDateFieldPolicy;
  };
  FormInputDateTime?: {
    keyFields?:
      | false
      | FormInputDateTimeKeySpecifier
      | (() => undefined | FormInputDateTimeKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputDateTimeFieldPolicy;
  };
  FormInputDecimal?: {
    keyFields?:
      | false
      | FormInputDecimalKeySpecifier
      | (() => undefined | FormInputDecimalKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputDecimalFieldPolicy;
  };
  FormInputMultiLangText?: {
    keyFields?:
      | false
      | FormInputMultiLangTextKeySpecifier
      | (() => undefined | FormInputMultiLangTextKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputMultiLangTextFieldPolicy;
  };
  FormInputText?: {
    keyFields?:
      | false
      | FormInputTextKeySpecifier
      | (() => undefined | FormInputTextKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputTextFieldPolicy;
  };
  FormInputUploadedFile?: {
    keyFields?:
      | false
      | FormInputUploadedFileKeySpecifier
      | (() => undefined | FormInputUploadedFileKeySpecifier);
    queryType?: true;
    mutationType?: true;
    subscriptionType?: true;
    fields?: FormInputUploadedFileFieldPolicy;
  };
};
