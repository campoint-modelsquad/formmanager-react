
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./react-campoint-forms.cjs.production.min.js')
} else {
  module.exports = require('./react-campoint-forms.cjs.development.js')
}
